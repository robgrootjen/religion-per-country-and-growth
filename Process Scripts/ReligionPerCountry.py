import requests
from bs4 import BeautifulSoup
import csv

#Request web content
result = requests.get('http://worldpopulationreview.com/countries/religion-by-country/')

#Save content in variable
src = result.content

#Soup
soup = BeautifulSoup(src, 'lxml')

#Look for table and save csv
table = soup.find('table')
with open ('countryreligion2019.csv', 'w', newline='') as f:
    writer = csv.writer(f)
    for tr in table('tr')[:-1]:
        row = [t.get_text(strip=True) for t in tr(['td','th'])]
        cell1 = row[0]
        cell2 = row[1].replace(',','').replace('<','')
        cell3 = row[2].replace(',','').replace('<','')
        cell4 = row[3].replace(',','').replace('<','')
        cell5 = row[4].replace(',','').replace('<','')
        cell6 = row[5].replace(',','').replace('<','')
        cell7 = row[6].replace(',','').replace('<','')
        cell8 = row[7].replace(',','').replace('<','')
        cell9 = row[8].replace(',','').replace('<','')
        cell10 = row[9].replace(',','').replace('<','')
        row = [cell1,cell2,cell3,cell4,cell5,cell6,cell7,cell8,cell9,cell10]
        writer.writerow(row)

