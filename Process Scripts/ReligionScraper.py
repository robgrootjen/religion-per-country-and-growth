import requests
from bs4 import BeautifulSoup  
import csv

#Request web content
result = requests.get('https://en.wikipedia.org/wiki/Growth_of_religion')

#Save content in variable
src = result.content

#Soup
soup = BeautifulSoup(src, 'lxml')

#Look for table and save in csv
table = soup.find('table')
with open('religiongrowth.csv','w',newline='') as f:
    writer = csv.writer(f)
    writer.writerow(('Religion','Followers in 1910','Followers in 2010'))
    for tr in table('tr')[2:-2]:
        row = [t.get_text(strip=True) for t in tr(['td','th'])]
        cell1 = row[0]
        cell2 = row[1].replace(',','')
        cell4 = row[3].replace(',','')
        row = [cell1, cell2, cell4]
        writer.writerow(row)




