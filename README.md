The datapackage includes how many followers in every religion and each country. And also the growth of every religion since 1910 and 2010.

## Data
The data is sourced from: 
* Historical data since 1910 - 2010 - https://en.wikipedia.org/wiki/Growth_of_religion
* Religion per country - http://worldpopulationreview.com/countries/religion-by-country/

The repo includes:
* datapackage.json
* countryreligion2019.csv
* religiongrowth.csv
* 2 scrapers.
    * ReligionPerCountry.py
    * ReligionScraper.py

## Preparation

***Requires:***
1. Python
2. Visualstudio / Jupyter notebook / or any platform that works with python.

***countryreligion2019.csv*** (Shows 2019 data of how many followers per religion in each and every country.)

***IMPORTANT:*** If cell says "10,000", it's "<=10,000". I removed the "<=" for coding/csv purposes.
* The CSV Data has 10 colummns. 
    * Country
    * Christians
    * Muslims
    * Unaffiliated
    * Hindus
    * Buddhists
    * Folk Religions
    * Other
    * Jews
    * Population 2019

***religiongrowth.csv*** (Shows historical data about growth per religion from 1910 and 2010.)
* The CSV Data has 3 colummns.
    * Religion
    * Followers in 1910
    * Followers in 2010

***datapackage.json***
* The json file has all the information from the two csv files, licence, author and 2 line graphs for every table.

***ReligionPerCountry.py***
* This script will scrape the current count of followers per religion in each country from this table - 
    * http://worldpopulationreview.com/countries/religion-by-country/

***ReligionScraper.py***
* This script will scrape the historical data from each religion in 1910 and 2010 from this table - 
    * https://en.wikipedia.org/wiki/Growth_of_religion 

***Instructions:***
* Copy any of the two script in the "Process Scripts" folder
* Open in jupyter notebook, python shell, VS code, or any preferred platform.
* Run the code
* Csv file will be saved in document where your terminal is at the moment.

## Licence
This Data Package is made available under the Public Domain Dedication and License v1.0 
